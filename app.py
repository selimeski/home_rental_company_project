from flask import Flask, request, render_template, redirect, url_for, jsonify

app = Flask(__name__)

database = {}
# user_database = {"Selim":"Selim_USER1"}
property_database = []
propertyid = {"id":1}
is_logged_in = False

@app.route("/", methods=["POST","GET"])
def index():
    return render_template("index.html")



@app.route('/toregister', methods=['POST'])
def toregister():
    return render_template("register.html")

@app.route('/tologin', methods=['POST'])
def tologin():
    return render_template("login.html")

@app.route('/property/tocreate/<string:user>', methods=['POST'])
def property_tocreate(user):
    return render_template("create.html", user=user)

@app.route('/property/toupdate/<string:user>', methods=['POST'])
def property_toupdate(user):
    return render_template("update.html", user=user)

@app.route('/property/todelete/<string:user>', methods=['POST'])
def property_todelete(user):
    return render_template("delete.html", user=user)


@app.route('/register', methods=['POST'])
def register():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        if database.get(username):
            # return "Username is already taken!"
            return render_template("notregistered.html")
        else:
            database.update({username: {"password": password, "properties": [], "bookings": [], "is_logged_in":False}})
            # user_database.update({username:username+"_USER1"})
            return render_template("isregistered.html", data=database[username], user=username)
    else:
        return redirect(url_for(index))

    # return "Buradan Register işlemi olacak."



@app.route('/login', methods=['POST'])
def login():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        if database.get(username) != None:
            if database.get(username).get("password") == password:
                # return "Login is successful!"
                # property_create(username)
                # return render_template("loggedin.html", user=user_database.get(username))
                database[username].update({"is_logged_in":True})
                return render_template("loggedin.html", data=database[username], user=username)
            else:
                return render_template("notloggedin.html")
        else:
            return render_template("notloggedin.html")
    # return "Buradan Login işlemi olacak."


@app.route('/logout/<string:user>', methods=['POST'])
def logout(user):
    if request.method == "POST":
        if database.get(user).get("is_logged_in") == True:
            database[user].update({"is_logged_in": False})
            return render_template("loggedout.html")
        else:
            return render_template("index.html")




@app.route('/properties', methods=['GET'])
def properties():
    if request.method == "GET":
        return render_template("allproperties.html",data=property_database)


@app.route('/property', methods=['POST'])
def property():
    if request.method == "POST":
        property_id = request.form.get("property_id")
        for property in property_database:
            if property["property_id"] == property_id:
            # if property_database.get(property):
                return render_template("aproperty.html", property=property_database[property_database.index(property)])
                # return render_template("aproperty.html", property=property_database[property])
        else:
            return render_template("notaproperty.html", property=property_id)

@app.route('/property/create/<string:user>', methods=['POST'])
def property_create(user):
    if request.method == "POST":
        if database.get(user).get("is_logged_in") == True:
            name = request.form.get("name")
            num_of_bedrooms = request.form.get("num_of_bedrooms")
            num_of_rooms = request.form.get("num_of_rooms")
            property_database.append({"property_id":user+"_"+str(propertyid.get("id")), "name":name, "num_of_bedrooms":num_of_bedrooms, "num_of_rooms":num_of_rooms})
            propertyid.update({"id":propertyid.get("id")+1})
            property_created = user+"_"+str(propertyid.get("id")-1)
            for property in property_database:
                if property["property_id"] == property_created:
                    property_info = property_database[property_database.index(property)]
            return  render_template("created.html", property_created=property_created, properties=property_database, user=user, property=property_info)
        # return "username"
    # return render_template("create.html")
    # return "Burada bir property oluşturulacak."
        else:
            return render_template("login.html")
    else:
        return render_template("login.html")

@app.route('/property/update/<string:user>', methods=['POST'])
def property_update(user):
    if request.method == "POST":
        if database.get(user).get("is_logged_in") == True:
            property_id = request.form.get("property_id")
            name = request.form.get("name")
            num_of_bedrooms = request.form.get("num_of_bedrooms")
            num_of_rooms = request.form.get("num_of_rooms")
            user_of_property = property_id.split("_")
            if user_of_property[0] == user:
                for property in property_database:
                    if property["property_id"] == property_id:
                    # if property_database.get(property_id):
                        property["name"] = name
                        property["num_of_bedrooms"] = num_of_bedrooms
                        property["num_of_rooms"] = num_of_rooms
                        # property_database[property_id].update({"name":name, "num_of_bedrooms":num_of_bedrooms, "num_of_rooms":num_of_rooms})
                        return render_template("updated.html", property_id=property_id, user=user)
            else:
                return render_template("cannotupdate.html", user=user)
        else:
            return render_template("cannotupdate.html", user=user)
        return render_template("cannotupdate.html", user=user)


@app.route('/property/delete/<string:user>', methods=['DELETE', 'POST'])
def property_delete(user):
    if request.method == "POST":
        if database.get(user).get("is_logged_in") == True:
            property_id = request.form.get("property_id")
            user_of_property = property_id.split("_")
            if user_of_property[0] == user:
                for property in property_database:
                    if property["property_id"] == property_id:
                        property_database.pop(property_database.index(property))
                        return render_template("deleted.html", property_id=property_id, user=user)
                        # property_database.pop(property_id)
            else:
                return render_template("cannotdelete.html", user=user)
        return render_template("cannotdelete.html", user=user)


if __name__ == '__main__':
    app.run()
